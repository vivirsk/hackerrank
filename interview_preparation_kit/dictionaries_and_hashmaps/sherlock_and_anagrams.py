#!/bin/python3
"""
Sherlock and Anagrams
"""
from collections import Counter

STRING_1 = "ifailuhkqq"     # --> 3
STRING_2 = "kkkk"           # --> 10
STRING_3 = "abcd"           # --> 0


def sherlockAndAnagrams(s):
    """
    Given a string, finds the number of pairs of substrings of the string
    which are anagrams of each other.

    Reason for window sliding:
    As two substrings establishing an anagram has the same length - with
    different sizes:
        -   from the shortest substring containing 1 string all the way up
        -   to n - 1

    :param s:       a string
    :return:        number of anagrams
    """
    num_of_anagrams = 0

    # window sliding
    for i in range(1, len(s)):
        # storing the resulting substrings on a "window-size level"
        # to be able to compare its elements with each other later
        list_of_dicts = []

        for start in range(0, len(s) - i + 1):
            end = start + i
            sub_string = s[start:end]
            # count the frequency of each letter
            counter = Counter(sub_string)
            list_of_dicts.append(counter)

        for num, counter_dict in enumerate(list_of_dicts):
            for another_dict in list_of_dicts[num+1:]+list_of_dicts[:num]:
                # compare dicts
                if counter_dict == another_dict:
                    num_of_anagrams += 1

    return num_of_anagrams // 2  # as each dict was compared twice


if __name__ == '__main__':
    print(sherlockAndAnagrams(STRING_2))
