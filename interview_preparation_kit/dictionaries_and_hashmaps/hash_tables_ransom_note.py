#!/bin/python3
"""
Hash Tables: Ransom Note
"""

from collections import Counter

MAGAZINE_1 = "give me one grand today night".split(" ")
NOTE_1 = "give one grand today".split(" ")
# --> Yes

MAGAZINE_2 = "two times three is not four".split(" ")
NOTE_2 = "two times two is four".split(" ")
# --> No

MAGAZINE_3 = "ive got a lovely bunch of coconuts".split(" ")
NOTE_3 = "ive got some coconuts".split(" ")
# --> No


def checkMagazine(magazine, note):
    """
    Given the words in the magazine and the words in the ransom note, prints
    Yes if the user can replicate his/her ransom note exactly using whole
    words from the magazine; otherwise, print No.

    :param magazine:    an array of strings, each a word in the magazine
    :param note:        an array of strings, each a word in the ransom note
    :return:            Yes if the note can be formed using the magazine, or
                        No
    """

    magazine_counter = Counter(magazine)
    note_counter = Counter(note)
    result = note_counter - magazine_counter

    return "No" if result else "Yes"


if __name__ == '__main__':
    print(checkMagazine(MAGAZINE_1, NOTE_1))
