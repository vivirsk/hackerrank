#!/bin/python3
"""
Two Strings
"""

S1_1 = "hello"
S2_1 = "world"

S1_2 = "hi"
S2_2 = "world"


def twoStrings(s1, s2):
    """
    Given two strings, determine if they share a common substring.
    A substring may be as small as one character.

    For example, the words "a", "and", "art" share the common substring "a".
    The words "be" and "cat" do not share a substring.

    :param s1:      string to analyze
    :param s2:      string to analyze
    :return:        a string, either YES or NO based on whether the strings
                    share a common substring.
    """
    result = any(letter in s2 for letter in s1)
    return "YES" if result else "NO"


if __name__ == '__main__':
    print(twoStrings(S1_2, S2_2))
