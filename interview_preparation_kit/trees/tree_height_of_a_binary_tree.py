""" Python program to find the maximum depth of tree """


# A binary tree node
class Node:
    # Constructor to create a new node
    def __init__(self, info):
        """ this is a node of the tree , which contains info as data, left , right """
        self.info = info
        self.left = None
        self.right = None


# Compute the "maxDepth" of a tree -- the number of nodes
# along the longest path from the root node down to the
# farthest leaf node
def height(node):
    if node is None:
        return -1  #

    # Compute the depth of each subtree
    left_height = height(node.left)
    right_height = height(node.right)

    # Use the larger one
    if left_height > right_height:
        return left_height + 1
    else:
        return right_height + 1


# Driver program to test above function
root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

print("Height of tree is %d" % (height(root)))
