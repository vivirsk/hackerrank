
def jumpingOnClouds(c):
    # +-------------------+-------------------+
    # | Space complexity  | Time complexity   |
    # |-------------------+-------------------+
    # | O(1)              | O(n)              |
    # +-------------------+-------------------+

    moves = 0
    position = 0 if not c[0] else 1

    while position < len(c) - 1:
        if position == len(c) - 2:
            position += 1
        elif not c[position+2]:
            position += 2
        else:  # only can jump 1
            position += 1
        moves += 1
    return moves


sample1 = [0, 1, 0, 0, 0, 1, 0]
sample2 = [0, 0, 0, 1, 0, 0]
print(jumpingOnClouds(sample2))
