#!/bin/python3


def rotLeft(a, d):
    i = d % len(a)
    return a[i:] + a[:i]


if __name__ == '__main__':

    nd = input().split()

    n = int(nd[0])

    d = int(nd[1])

    a = list(map(int, input().rstrip().split()))

    result = rotLeft(a, d)
    print(' '.join(map(str, result)))
