"""
2D Array - DS
"""


ARR = \
    """
    1 1 1 0 0 0
    0 1 0 0 0 0
    1 1 1 0 0 0
    0 0 2 4 4 0
    0 0 0 2 0 0
    0 0 1 2 4 0
    """


def hourglassSum(arr):
    """
    Given a 6x6 2D Array, arr

    We define an hourglass in A to be a subset of values with indices falling
    in this pattern in arr's graphical representation:

    a b c       --> start
      d         --> middle
    e f g       --> end

    There are 16 hourglasses in arr, and an hourglass sum is the sum of an
    hourglass' values.
    Calculates the hourglass sum for every hourglass in arr, then prints the
    maximum hourglass sum.

    :param arr:     an array of integers
    :return:        an integer, the maximum hourglass sum in the array
    """
    result = -63  # possible lowest value: 7 * -9 = -63

    for j in range(0, len(arr)-2):
        for i in range(0, len(arr) - 2):

            start = arr[i][j:(j+3)]
            middle = arr[i+1][j+1]
            end = arr[i+2][j:(j+3)]

            result = max(result, sum(start + [middle] + end))

    return result


if __name__ == '__main__':

    array = []
    for line in ARR.split("\n"):
        row = [int(item) for item in line.split(" ") if item != '']
        if row:
            array.append(row)

    print(hourglassSum(array))
