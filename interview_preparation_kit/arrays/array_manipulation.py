#!/bin/python3


def arrayManipulation(n, queries):
    arr = [0] * (n + 1)
    result = float('-inf')

    for query in queries:
        arr[query[0] - 1] += query[2]
        arr[query[1]] -= query[2]
    for i in range(1, n+1):
        arr[i] += arr[i-1]
        result = max(result, arr[i])

    return result


test_case1 = [
    [1, 5, 3],
    [4, 8, 7],
    [6, 9, 1]
]
test_case2 = [
    [1, 2, 100],
    [2, 5, 100],
    [3, 4, 100]
]
test_case3 = [
    [2, 6, 8],
    [3, 5, 7],
    [1, 8, 1],
    [5, 9, 15]
]

print(arrayManipulation(10, test_case2))
