#!/bin/python3
"""
Minimum Swaps 2
"""

ARR_1 = [7, 1, 3, 2, 4, 5, 6]   # --> 5
ARR_2 = [4, 3, 1, 2]            # --> 3
ARR_3 = [2, 3, 4, 1, 5]         # --> 3
ARR_4 = [2, 4, 1, 5, 3, 8, 6, 7]


def minimumSwaps(arr):
    """
    Finds the minimum number of swaps required to sort the array in ascending
    order.
    (You are allowed to swap any two elements.)

    :param arr:     an unordered array of consisting of consecutive integers
                    - without any duplicates
    :return:        an integer representing the minimum number of swaps to sort
                    the array

    E.g.:
    i   arr                         swap (indices)
    0   [7, 1, 3, 2, 4, 5, 6]   swap (0,3)
    1   [2, 1, 3, 7, 4, 5, 6]   swap (0,1)
    2   [1, 2, 3, 7, 4, 5, 6]   swap (3,4)
    3   [1, 2, 3, 4, 7, 5, 6]   swap (4,5)
    4   [1, 2, 3, 4, 5, 7, 6]   swap (5,6)
    5   [1, 2, 3, 4, 5, 6, 7]

    --> It took 5 swaps to sort the array.
    """
    counter = 0
    swaped = False

    for i in range(len(arr) - 1):
        for num, val in enumerate(arr):
            if num == val - 1:
                continue
            else:
                arr[val - 1], arr[num] = val, arr[val - 1]
                counter += 1
                swaped = True
            print(num, arr)
        if swaped:
            swaped = False
        else:
            break

    return counter


def minimumSwaps2(lst):

    counter = 0

    for i in range(len(lst)):

        while lst[i] != i+1:

            temp = lst[i] - 1
            lst[i], lst[temp] = lst[temp], lst[i]
            counter += 1

        if lst == sorted(lst):
            break
    return counter


if __name__ == '__main__':
    print(minimumSwaps(ARR_1))
    print('---')
    print(minimumSwaps2(ARR_4))
