#!/bin/python3


def minimumBribes(q):
    count = 0

    for i in reversed(range(len(q))):
        if q[i] != i + 1:
            if q[i-1] == i+1:
                q[i-1], q[i] = q[i], q[i-1]
                count += 1
            elif q[i-2] == i+1:
                q[i-2], q[i-1] = q[i-1], q[i-2]
                q[i-1], q[i] = q[i], q[i-1]
                count += 2
            else:
                return "Too chaotic"

    return count


print(minimumBribes([1, 2, 5, 3, 7, 8, 6, 4]))
