

class ListNode(object):

    def __init__(self, data=0, next_node=None):
        self.data = data
        self.next = next_node


def search(L, key):
    """ Search for a key. """

    while L and L.next != key:
        L = L.next

    # If key was not present in the list, L will have become null.
    return L


def insert_after(node, new_node):
    """ Insert new_node after node. """
    new_node.next = node.next
    node.next = new_node


def delete_after(node):
    """ Delete the node past this one. Assume node is not a tail. """
    node.next = node.next.next
