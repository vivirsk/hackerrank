#!/bin/python3


def aVeryBigSum(n, ar):
    return sum(ar)


n = int(input().strip())
ar = map(input().strip().split(' '))
result = aVeryBigSum(n, ar)
print(result)
