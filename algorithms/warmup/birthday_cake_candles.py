#!/bin/python3


def birthdayCakeCandles(n, ar):
    max_num = max(ar)
    return sum(i==max_num for i in ar)


n = int(input().strip())
ar = list(map(int, input().strip().split(' ')))
result = birthdayCakeCandles(n, sorted(ar))
print(result)
