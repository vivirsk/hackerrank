#!/bin/python


def strangeCounter(t):
    num = 0
    for i in range(t):
        if num + 1 + 3*(2**i) > t > num:
            for n, counter_n in zip(range(num + 1, num + 1 + 3*(2**i)), reversed(range(1, 3*(2**i)+1))):
                if t == n:
                    return counter_n
        num += 3*(2**i)


print(strangeCounter(14))
