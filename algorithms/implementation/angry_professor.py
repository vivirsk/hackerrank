#!/bin/python3


def angryProfessor(k, a):
    return "NO" \
        if len([i for i in filter(lambda x: x <= 0, sorted(a))]) >= k \
        else "YES"


def angryProfessor2(k, a):
    return "NO" \
        if len([x for x in sorted(a) if x <= 0]) >= k \
        else "YES"


print(angryProfessor(3, [-1, -3, 4, 2]))
print(angryProfessor(2, [0, -1, 2, 1]))

