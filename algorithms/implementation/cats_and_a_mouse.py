#!/bin/python3
"""

"""
import os
import sys


def catAndMouse(x, y, z):
    x_dist, y_dist = abs(x - z), abs(y - z)
    if x_dist != y_dist:
        if min(x_dist, y_dist) == x_dist:
            return "Cat A"
        else:
            return "Cat B"
    else:
        return "Mouse C"
