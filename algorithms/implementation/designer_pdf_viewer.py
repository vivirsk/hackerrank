

def designerPdfViewer(h, word):
    abc = list(map(chr, range(97, 123)))
    d = {}
    for num, letter in zip(h, abc):
        d[letter] = num
    highest = max([d[w] for w in word])

    return len(word) * highest


def designerPdfViewer2(h, word):
    height = 0
    for w in word:
        height = max(height, h[ord(w) - ord("a")])

    return len(word) * height


h = list(map(int, input().rstrip().split()))
word = input()
result = designerPdfViewer2(h, word)
print(result)
# 1 3 1 3 1 4 1 3 2 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 7
# zaba
