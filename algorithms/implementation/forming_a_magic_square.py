#!/bin/python3
"""
I don't like hardcoded version.
You can find a lot of topic discussing how to form magic squares using the
basic method:

    1. diagonally up and to the right when you can,
    2. down if you cannot.

This results in the following schema:
-------------------
| n+3 | n-4 | n+1 |
------------------
| n-2 |  n  | n+2 |
------------------
| n-1 | n+4 | n-3 |
-------------------

This approach has its limitation, as it works until n isn't the element we
have to change.
"""
from itertools import combinations, permutations
import sys


def formingMagicSquare(s):
    result = 0
    res = []
    n = s[1][1]     # get the middle value alias n
    row1 = [n-1, n+4, n-3]
    row2 = [n-2, n, n+2]
    row3 = [n+3, n-4, n+1]
    r = [row1, row2, row3]

    # all_r = list(permutations(r, 3))
    print(r)
    for i in range(3):
        for j in range(3):
            result += abs(r[i][j] - s[i][j])
    return result


if __name__ == "__main__":
    s = []
    for s_i in range(3):
       s_t = [int(s_temp) for s_temp in input().strip().split(' ')]
       s.append(s_t)
    result = formingMagicSquare(s)
    print(result)

#4 9 2
#3 5 7
#8 1 5

#4 8 2
#4 5 7
#6 1 6
'''
6 7 8
7 6 2
3 2 3'''