from collections import defaultdict


def happyLadyBugs(str):
    dct = defaultdict(list)

    for num, i in enumerate(str):
        dct[i].append(num)

    for k, v in dct.items():
        if k != '_' and len(v) < 2:
            return "NO"
        if v != range(v[0], v[-1]+1) and '_' not in dct.keys():
            return "NO"
    return "YES"


s0 = '_'
s1 = 'RBRB'
s2 = 'RRRR'
s3 = 'BBB'
s4 = 'BBB_'
print(happyLadyBugs(s0))
print(happyLadyBugs(s1))
print(happyLadyBugs(s2))
print(happyLadyBugs(s3))
print(happyLadyBugs(s4))
