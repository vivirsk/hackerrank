#!/bin/python3
"""
6
4 6 5 3 3 1
"""

import sys
import functools


def pickingNumbers(arr):
    return max(a.count(x) + a.count(x + 1) for x in set(a))


if __name__ == "__main__":
    n = int(input().strip())
    a = list(map(int, input().strip().split(' ')))
    result = pickingNumbers(a)
    print(result)
