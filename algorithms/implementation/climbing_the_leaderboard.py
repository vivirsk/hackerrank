#!/bin/python3

import os
import sys


def climbingLeaderboard(scores, alice):
    """

    """
    alice_result = []
    sorted_scores = sorted(list(set(scores)), reverse=True)
    l_sc = len(sorted_scores)

    for num in alice:
        while l_sc > 0 and num >= sorted_scores[l_sc - 1]:
            l_sc -= 1
        alice_result.append(l_sc + 1)
        """
        if num < sorted_scores[-1]:
            alice_result.append(len(sorted_scores)+1)
            continue
        for n, score in enumerate(sorted_scores):
            if num >= score:
                alice_result.append(n+1)
                break"""
    return alice_result


if __name__ == '__main__':

    scores_count = int(input())

    scores = list(map(int, input().rstrip().split()))

    alice_count = int(input())

    alice = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(scores, alice)
    print(result)

