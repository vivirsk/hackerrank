#!/bin/python3


def utopianTree(n):
    res = 0
    for i in range(int((n+1)/2)):
        res = (res + 1) * 2
    if n % 2 == 1:
        return res
    else:
        res += 1
        return res


print(utopianTree(7))
