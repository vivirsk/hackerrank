def beautiful_days(i, j, k):
    num = 0
    for item in range(i, j+1):
        reverse = int(str(item)[::-1])
        result = abs(item - reverse) % k
        if not result:
            num += 1
    return num


print(beautiful_days(20, 23, 6))
