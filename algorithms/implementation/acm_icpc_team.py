def acmTeam(topic):
    max_val = 0
    counter = 0

    for num_i, i in enumerate(topic):
        for num_j in range(num_i + 1, len(topic)):
            current_val = int(str(bin(int(i, 2) | int(topic[num_j], 2))).count('1'))
            print(current_val)
            if current_val > max_val:
                max_val = current_val
                counter = 1
            elif current_val == max_val:
                counter += 1
            else:
                continue

    return max_val, counter


topic1 = [
    '11101',
    '10101',
    '11001',
    '10111',
    '10000',
    '01110'
]
print(acmTeam(topic1))
