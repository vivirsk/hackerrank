#!/bin/python3


def circularArrayRotation(a, k, queries):
    for i in range(k):
        a.insert(0, a[-1 - i])
    result = [a[:-k][query] for query in queries]
    return result


print(circularArrayRotation([1, 2, 3], 2, [0, 1, 2]))

