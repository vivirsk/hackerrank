def bigger_is_greater(input_word):

    word = [letter for letter in input_word]
    pointer = len(word) - 2
    while pointer >= 0 and ord(word[pointer]) >= ord(word[pointer + 1]):
        pointer -= 1

    if pointer == -1:
        return "no answer"

    for i in reversed(range(pointer + 1, len(word))):
        if word[i] > word[pointer]:
            word[i], word[pointer] = word[pointer], word[i]
            break

    word[pointer + 1:] = reversed(word[pointer + 1:])
    return "".join(word) if "".join(word) != input_word else "no answer"


sample = """
lmno
dcba
dcbb
abdc
abcd
"""
for s in sample.split():
    print(s)
    print(bigger_is_greater(s))
