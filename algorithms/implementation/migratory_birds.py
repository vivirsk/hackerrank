#!/bin/python3

"""
A flock of n birds is flying across the continent. Each bird has a type, and
the different types are designated by the ID numbers 1, 2, 3, 4, and 5.
"""


def migratoryBirds(n, ar):
    counter = [0] * 6
    for i in ar:
        counter[i] += 1
    return counter.index(max(counter))


n = int(input().strip())
ar = list(map(int, input().strip().split(' ')))
result = migratoryBirds(n, ar)
print(result)

# ar = [1, 4, 4, 4, 2, 2, 2, 5, 5, 5, 3]
