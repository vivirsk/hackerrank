

def larrysArray(arr):
    pointer = 0
    length = len(arr)

    def rotate_left(lst, j, dist):
        if dist == 1:
            lst[j-1], lst[j], lst[j+1] = lst[j], lst[j+1], lst[j-1]
            return lst, j-1
        else:   # d = 2
            lst[j-2], lst[j-1], lst[j] = lst[j], lst[j-2], lst[j-1]
            return lst, j-2

    for i in range(1, length+1):
        if i in arr:
            current_index = arr.index(i)
        else:
            return "NO"
        # print(current_index)

        while current_index != pointer:
            if arr.count('T') == len(arr) - 2 and current_index == len(arr) - 1:
                return "NO"
            elif current_index - pointer == 1:  # arr[current_index - 2] == 'T':
                d = 1
            else:
                d = 2
            # print(arr, current_index)
            arr, current_index = rotate_left(arr, current_index, d)

        arr[pointer] = 'T'
        pointer += 1
    return "YES"


a1 = [1, 6, 5, 2, 4, 3]
a2 = [3, 1, 2]
a3 = [1, 3, 4, 2]
a4 = [1, 2, 3, 5, 4]
a5 = [1, 6, 5, 2, 3, 4]
a6 = [7, 11, 8, 13]

print(larrysArray(a5))
