from collections import defaultdict

# time complexity:  O(K)


def queensAttack(n, k, r_q, c_q, obstacles=None):

    dct = defaultdict(int)

    def get_direction(r_o, c_o):
        drct = None
        if r_o == 0 and c_o > 0: drct = 'r'
        if r_o == 0 and c_o < 0: drct = 'l'
        if c_o == 0 and r_o > 0: drct = 'u'
        if c_o == 0 and r_o < 0: drct = 'd'
        if r_o > 0 > c_o: drct = 'lu'
        if r_o > 0 and c_o > 0: drct = 'ru'
        if r_o < 0 and c_o < 0: drct = 'ld'
        if r_o < 0 < c_o: drct = 'rd'
        return drct

    if k:
        for obs in obstacles:

            r_o = obs[0] - r_q
            c_o = obs[1] - c_q

            # check if obstacle is in the way
            if r_o and c_o and abs(r_o) != abs(c_o):
                continue
            else:

                if r_o > 0:
                    steps_r_o = n - obs[0]
                elif r_o < 0:
                    steps_r_o = obs[0] - 1
                else:
                    steps_r_o = float("inf")

                if c_o > 0:
                    steps_c_o = n - obs[1]
                elif c_o < 0:
                    steps_c_o = obs[1] - 1
                else:
                    steps_c_o = float("inf")

                pos = get_direction(r_o, c_o)
                if dct[pos]:
                    dct[pos] = max(dct[pos], min(steps_r_o, steps_c_o) + 1)
                else:
                    dct[pos] = min(steps_r_o, steps_c_o) + 1

    all = (n - 1) * 2 + min(n-c_q, r_q-1) + min(n-r_q, c_q-1) + min(r_q-1, c_q-1) + min(n-r_q, n-c_q)
    for k in dct:
        all -= dct[k]
    return all


print(queensAttack(4, 0, 4, 4))
print(queensAttack(5, 3, 4, 3, [(5,5), (4,2), (2,3)]))
print(queensAttack(1, 0, 1, 1))

