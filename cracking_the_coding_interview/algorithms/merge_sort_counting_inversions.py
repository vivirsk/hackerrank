#!/bin/python3
"""
My implementation of the counting inversions algorithm.
This is for algorithm learning purposes only.
"""


def Merge_and_CountSplitInv(left_half, right_half):
    """
    Merge revisited.
    The idea is to design our recursive inversion-counting algorithm so that
    it piggybacks on the MergeSort algorithm.

    Each recursive call will be responsible for:
        - counting the number of inversions in the list that it is given,
        - returning a sorted version of the list.

    :param left_half:       sorted list C
    :param right_half:      sorted list D
    :return:                sorted list B (length n) and the number of split
                            inversions
    """
    i = 0
    j = 0
    splitInv = 0

    B = []

    while i < len(left_half) and j < len(right_half):

        if left_half[i] <= right_half[j]:
            B.append(left_half[i])
            i += 1
        else:
            B.append(right_half[j])
            j += 1
            splitInv += (len(left_half) - i)

    B += left_half[i:]
    B += right_half[j:]

    print("result: ", B)
    return B, splitInv


def Sort_and_CountInv(A):
    """
    Input:      list A of n distinct integers.
    Output:     list with the same integers, sorted from smallest to largest.

    :return:    Output
    """

    # base case
    if len(A) < 2:
        return A, 0

    # divide the list into two
    mid = len(A) // 2

    left = A[:mid]      # recursively sort first half of A
    right = A[mid:]     # recursively sort second half of A

    x, leftInv = Sort_and_CountInv(left)
    y, rightInv = Sort_and_CountInv(right)
    z, splitInv = Merge_and_CountSplitInv(x, y)

    return z, leftInv + rightInv + splitInv


print(Sort_and_CountInv([1, 3, 2, 4, 6, 5])[1])
print(Sort_and_CountInv([4, 5, 6, 7, 1, 2, 3])[1])
