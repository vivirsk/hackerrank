#!/bin/python3


# Complete the compareTriplets function below.
def compareTriplets(a, b):
    results = [0, 0]

    for x, y in zip(a, b):
        if x > y:
            results[0] += 1
        if y > x:
            results[1] += 1

    return results


print(compareTriplets([5, 6, 7], [3, 6, 10]))
